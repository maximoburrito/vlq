
(ns vlq.core
  (:require [clojure.string :as string]))

(def BASE64-CHARS "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=")

(defn encode-number [n-to-encode]
  (let [[npos sign-bit]
        (if (neg? n-to-encode)
          [(- n-to-encode) 1]
          [n-to-encode 0])]
    (loop [n (bit-shift-right npos 4)
           d (-> npos
               (bit-and 15)
               (bit-shift-left 1)
               (bit-xor sign-bit))
           encoded ""]
      (if (> n 0)
        (recur
          (bit-shift-right n 5)
          (bit-and n 31)
          (str encoded (get BASE64-CHARS (bit-or 32 d))))
        (str encoded (get BASE64-CHARS d))))))

(defn encode [o]
  (if (number? o)
    (encode-number o)
    (reduce str (map encode o))))


(defn partition-after [pred coll]
  (when (seq coll)
    (let [[front back] (split-with pred coll)]
      (lazy-seq
        (cons
          (concat front (take 1 back))
          (partition-after pred (rest back)))))))


(defn extract-one [encoded]
  (loop [c (seq encoded)
         n 0]
    (let [d (.indexOf BASE64-CHARS (str (first c)))]
      (cond
        (< d 0)  ;; invalid encoding
        nil

        (< d 32) ;; not a continuation
        (let [final
              (-> n
                (bit-shift-left 5)
                (bit-xor (bit-shift-right d 1)))]
          (if (pos? (bit-and d 1))
            (- final)
            final))

        :else ;; continuation
        (recur
          (rest encoded)
          (-> n
            (bit-shift-left 5)
            (bit-xor (bit-and 31 d))))))))


(defn decode-number [encoded]
  (let [n
        (loop [c (seq encoded)
                bits 0
                n 0]
           (if (empty? c)
             n
             (recur
               (rest c)
               (+ bits 5)
               (-> (bit-and 31 (.indexOf BASE64-CHARS (str (first c))))
                 (bit-shift-left bits)
                 (bit-xor n)))))]
    ;; LSB is sign
    (if (pos? (bit-and 1 n))
      (- (bit-shift-right n 1))
      (bit-shift-right n 1))))



(defn is-continuation [c]
  (pos? (bit-and 32 (.indexOf BASE64-CHARS (str c)))))

(defn decode [encoded]
  (into []
    (for [part (partition-after is-continuation encoded)]
      (decode-number (apply str part)))))

(defn decode-sourcemap [text]
  (into []
    (for [line (string/split text #";")]
      (into []
        (for [mapping (string/split line #",")]
          (decode mapping))))))


(decode-sourcemap "CAAC,IAAI,IAAM,SAAUA,GAClB,OAAOC,IAAID;CCDb,IAAI,IAAM,SAAUE,GAClB,OAAOA")





